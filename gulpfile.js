var gulp = require('gulp');
var sass = require('gulp-sass');
var eslint = require('gulp-eslint');
var htmllint = require('gulp-htmllint');
var minifyCSS = require('gulp-minify-css');
var named = require('vinyl-named');
var webpack = require('webpack-stream');

var srcFolder = './src';
var testFolder = './test';
var buildFolder = './build';

var paths = {
    style: {
        src: srcFolder + '/style/*.sass',
        output: buildFolder + '/style',
    },
    pluginjs: {
        src: srcFolder + '/**/*.js',
    },
    pagejs: {
        src: [
            srcFolder + '/js/home.js',
            srcFolder + '/js/popup.js',
            srcFolder + '/js/session.js',
            srcFolder + '/js/sessions.js',
            srcFolder + '/js/website.js',
            srcFolder + '/js/detail_cumul.js',
        ],
        output: buildFolder + '/js',
    },
    backgroundjs: {
        src: srcFolder + '/background.js',
        output: buildFolder,
    },
    testjs: {
        src: testFolder + '/**/*.js',
    },
    html: {
        src: srcFolder + '/page/**/*.html',
    },
    fafonts: {
        src: './node_modules/@fortawesome/fontawesome-free/webfonts/**/*',
        output: buildFolder + '/fonts/font-awesome',
    },
    structure: {
        src: [
            './manifest.json',
            srcFolder + '/**/*.html',
            srcFolder + '/**/*.png',
        ],
    },
};

var webpackOptions = {
    mode: 'production',
    optimization: {
        minimize: false,
    },
};

gulp.task('backgroundjs', function () {
    return gulp.src(paths.backgroundjs.src)
        .pipe(named())
        .pipe(webpack(webpackOptions))
        .pipe(gulp.dest(paths.backgroundjs.output));
});

gulp.task('pagejs', function() {
    return gulp.src(paths.pagejs.src)
        .pipe(named())
        .pipe(webpack(webpackOptions))
        .pipe(gulp.dest(paths.pagejs.output))
});

gulp.task('js', gulp.series('backgroundjs', 'pagejs'));
gulp.task('js:watch', function () {
    gulp.watch(paths.pluginjs.src, gulp.series('js'));
});

gulp.task('sass', function () {
    return gulp.src(paths.style.src)
        .pipe(sass({includePaths: ['./node_modules']}))
        .pipe(minifyCSS({
            relativeTo: './node_modules',
            processImport: true
        }))
        .pipe(gulp.dest(paths.style.output))
});

gulp.task('fonts', function() {
    return gulp.src(paths.fafonts.src)
        .pipe(gulp.dest(paths.fafonts.output));
});

gulp.task('sass:watch', function () {
    gulp.watch(paths.style.src, gulp.series('sass'));
});

gulp.task('eslint', function () {
    return gulp.src([paths.pluginjs.src, paths.testjs.src])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('eslint:watch', function () {
    gulp.watch(paths.pluginjs.src, gulp.series('eslint'));
});

gulp.task('htmllint', function () {
    return gulp.src(paths.html.src)
        .pipe(htmllint({
            config: '.htmllint.json',
            failOnError: true,
        }));
});

gulp.task('structure', function () {
    return gulp.src(paths.structure.src)
        .pipe(gulp.dest(buildFolder));
});

gulp.task('structure:watch', function () {
    gulp.watch(paths.structure.src, gulp.series('structure'));
});

gulp.task('htmllint:watch', function () {
    gulp.watch(paths.html.src, gulp.series('htmllint'));
});

gulp.task('lint', gulp.parallel('eslint', 'htmllint'));
gulp.task('lint:watch', gulp.parallel('eslint:watch', 'htmllint:watch'));

gulp.task('build', gulp.parallel('sass', 'fonts', 'js', 'structure'));
gulp.task('build:watch', gulp.parallel('sass:watch', 'js:watch', 'structure:watch'));
