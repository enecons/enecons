import test from 'ava';

import Formatter from '../src/formatter.js';

function size(t, input_value, input_unit, expected_value, expected_unit) {
    var formatter = new Formatter();

    var formatted = formatter.formatSize(input_value, input_unit);

    t.is(formatted.value, expected_value);
    t.is(formatted.unit, expected_unit);
}

size.title = (_, input_value, input_unit) => {
    return `formatSize: ${input_value} ${input_unit}`;
};

test(size, 0, 'B', '0', 'B');
test(size, 0, 'MB', '0', 'B');
test(size, 0, 'GB', '0', 'B');
test(size, 42, 'B', '42', 'B');
test(size, 150, 'B', '150', 'B');
test(size, 1000, 'B', '1', 'KB');
test(size, 4200, 'B', '4.2', 'KB');
test(size, 4242, 'B', '4.24', 'KB');
test(size, 14242, 'B', '14.2', 'KB');
test(size, 114242, 'B', '114', 'KB');
test(size, 114642, 'B', '115', 'KB');
test(size, 4200, 'KB', '4.2', 'MB');
test(size, 4242, 'KB', '4.24', 'MB');
test(size, 14242, 'KB', '14.2', 'MB');
test(size, 114242, 'KB', '114', 'MB');
test(size, 114642, 'KB', '115', 'MB');
test(size, 4200000, 'B', '4.2', 'MB');
test(size, 4242000, 'B', '4.24', 'MB');
test(size, 14242000, 'B', '14.2', 'MB');
test(size, 114242000, 'B', '114', 'MB');
test(size, 114642000, 'B', '115', 'MB');
test(size, 0.0001, 'B', '0.01', 'B');
test(size, 0.042, 'B', '0.05', 'B');
test(size, 4242.042, 'B', '4.24', 'KB');

function duration(t, input_value, input_unit, expected_value, expected_unit) {
    var formatter = new Formatter();

    var formatted = formatter.formatDuration(input_value, input_unit);

    t.is(formatted.value, expected_value);
    t.is(formatted.unit, expected_unit);
}

duration.title = (_, input_value, input_unit) => {
    return `formatDuration: ${input_value} ${input_unit}`;
};

test(duration, 0, 's', '0', 's');
test(duration, 60, 's', '1', 'm');
test(duration, 65, 'm', '1', 'h');
test(duration, 70, 'm', '1', 'h');
test(duration, 120, 'm', '2', 'h');
test(duration, 24 * 60, 'm', '1', 'd');
