import test from 'ava';
import sinon from 'sinon';

import Calculator from '../src/calculator.js';
import Db from '../src/db/db.js';
import Asset from '../src/db/asset.js';

test.before(t => {
    var db = sinon.createStubInstance(Db);
    var calculator = new Calculator(db);

    t.context.db = db;
    t.context.calculator = calculator;
});

test('basicWebsiteCarbonEmission', t => {
    var db = t.context.db;
    var calculator = t.context.calculator;

    var assetsSizes = [100000, 500000, 4242424242, 999999999];
    var assets = [];

    for (let i = 0; i < assetsSizes.length; i++) {
        let asset = sinon.createStubInstance(Asset);
        asset.getNetworkSize.returns(assetsSizes[i]);
        assets.push(asset);
    }

    db.getAssetsByWebsiteId.withArgs(42).returns(assets);

    return calculator.getWebsiteCarbonEmission(42).then(co2 => {
        t.is(Math.round(co2), 1208);
    });
});
