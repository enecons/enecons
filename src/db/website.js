export default class Website {
    constructor(url, date, session_id) {
        this.url = url;
        this.date = date;
        this.session_id = session_id;
    }
}
