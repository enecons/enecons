export default class Asset {
    constructor(url, size, dom_elements, type, cached, website_id) {
        this.url = url;
        this.size = size;
        this.dom_elements = dom_elements;
        this.type = type;
        this.cached = cached;
        this.website_id = website_id;
    }

    getNetworkSize() {
        if (this.cached) {
            return 0;
        }
        return this.size;
    }
}
