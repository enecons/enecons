import Dexie from 'dexie';
import Session from './session.js';
import Website from './website.js';
import Asset from './asset.js';

// simulate a kind of `ORM'
export default class Db {
    constructor() {
        this.db = new Dexie('db');

        this.db.version(1).stores({
            session: '++id,begin,end',
            website: '++id,date,session_id',
            asset: '++id,website_id',
        });

        this.db.session.mapToClass(Session);
        this.db.website.mapToClass(Website);
        this.db.asset.mapToClass(Asset);
    }

    async save(object) {
        return this.db[object.constructor.name.toLowerCase()].put(object)
            .then(function(id) {
                object.id = id;

                return object;
            });
    }

    async getAssetsByWebsiteId(website_id) {
        return this.db.asset.where('website_id').equals(website_id).toArray();
    }

    async getAllSessions() {
        return this.db.session.toArray();
    }

    async getWebsitesBySessionId(session_id) {
        return this.db.website.where('session_id').equals(session_id).toArray();
    }

    async getWebsitesByTimeWindow(start, end) {
        return this.db.website.where('date')
            .between(start.getTime(), end.getTime(), true, false).toArray();
    }

    async countAssetsByWebsiteId(website_id) {
        return this.db.asset.where('website_id').equals(website_id).count();
    }

    async getWebsiteByWebsiteId(website_id) {
        return this.db.website.where('id').equals(website_id).toArray();
    }

    async countWebsitesBySessionId(session_id) {
        return this.db.website.where('session_id').equals(session_id).count();
    }
}
