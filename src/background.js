import Db from './db/db.js';
import Website from './db/website.js';
import Asset from './db/asset.js';
import app from './app.js';

var db = new Db();
window.pluginApp = app;

app.getCurrentSession().then(function(current_session) {
    browser.webRequest.onCompleted.addListener(function(request) {
        if (request.tabId === -1) {
            return;
        }

        if (request.type === 'main_frame') {
            let website = new Website(
                request.url,
                Date.now(),
                current_session.id);
            website = db.save(website);
            app.setWebsiteOnTab(request.tabId, website);
        }

        if (!app.hasWebsiteOnTab(request.tabId)) {
            return;
        }

        app.getWebsiteOnTab(request.tabId).then(function(current_website) {
            var filtered_len = request.responseHeaders.filter(
                res => res.name === 'content-length');
            var filtered_contenttype = request.responseHeaders.filter(
                res => res.name === 'content-type');

            var content_type = null;
            if (filtered_contenttype.length !== 0) {
                content_type = filtered_contenttype[0].value;
            }
            var size = null;
            if (filtered_len.length !== 0) {
                size = parseInt(filtered_len[0].value, 10);
            }
            var asset = new Asset(request.url,
                size,
                null,
                content_type,
                request.fromCache,
                current_website.id);

            db.save(asset);
        });
    },
    {urls: ['<all_urls>']},
    ['responseHeaders']);
});
