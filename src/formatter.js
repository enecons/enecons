export default class Formatter {
    constructor() {
        this.units = {
            size: ['B', 'KB', 'MB', 'GB'],
            mass: ['g', 'kg', 't'],
            distance: ['mm', 'm', 'km'],
            duration: ['s', 'm', 'h', 'd'],
        };

        this.units_mul = {
            size: [1, 1e3, 1e6, 1e9, Infinity],
            mass: [1, 1e3, 1e6, Infinity],
            distance: [1, 1e3, 1e6, Infinity],
            duration: [1, 60, 3600, 24 * 3600, Infinity],
        };

        Object.freeze(this.units);
        Object.freeze(this.units_mul);
    };

    formatSize(size, unit = 'B') {
        return this.formatUnit(size, unit, 'size');
    }

    formatMass(mass, unit = 'g') {
        return this.formatUnit(mass, unit, 'mass');
    }

    formatDistance(distance, unit = 'mm') {
        return this.formatUnit(distance, unit, 'distance');
    }

    formatDuration(duration, unit = 's') {
        return this.formatUnit(duration, unit, 'duration');
    }

    formatUnit(value, unit, base) {
        var units = this.units[base];
        var units_mul = this.units_mul[base];
        var base_index = units.findIndex(key => key === unit);

        value *= this.units_mul[base][base_index];

        var final_base_index = null;
        for (var i = 1; i < units_mul.length; i++) {
            if (value >= units_mul[i]) {
                continue;
            }

            value /= units_mul[i - 1];
            final_base_index = i - 1;
            break;
        }

        value = (Math.ceil(value.toPrecision(3) * 100) / 100);

        if (base === 'duration') {
            value = Math.floor(value);
        }

        return {
            value: value.toLocaleString(),
            unit: units[final_base_index],
        };
    }

    formatDate(date) {
        var options = { weekday: 'long', year: 'numeric',
            month: 'long', day: 'numeric' };
        return date.toLocaleDateString('default', options);
    }

    formatDateTime(date) {
        var options = { weekday: 'long', year: 'numeric',
            month: 'long', day: 'numeric',
            hour: 'numeric', minute: 'numeric', second: 'numeric' };
        return date.toLocaleString('default', options);
    }
}
