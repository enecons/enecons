import 'bootstrap';

import Db from '../db/db.js';
import Calculator from '../calculator.js';
import Formatter from '../formatter.js';

var db = new Db();
var calculator = new Calculator(db);
var formatter = new Formatter();

var sessionNode = document.getElementById('session');
var websiteNode = document.getElementById('website');

function showEmptyOrNonEmpty(node, nonEmpty) {
    node.getElementsByClassName((nonEmpty ? 'non-' : '') + 'empty-section')[0]
        .classList
        .remove('d-none');
}

function setMetric(
    node,
    metric,
    value,
    unit = null,
    desc = null,
    title = null) {
    var metricNode = node.getElementsByClassName('metric-' + metric)[0];
    var metricValueNode = metricNode.getElementsByClassName('value')[0];
    var metricValueValNode = metricValueNode.getElementsByClassName('val')[0];
    var metricValueUnitNode = metricValueNode.getElementsByClassName('unit')[0];
    var metricDescNode = metricNode.getElementsByClassName('description')[0];

    metricValueValNode.textContent = value;

    if (unit !== null) {
        metricValueUnitNode.textContent = unit;
    }

    if (desc !== null) {
        metricDescNode.textContent = desc;
    }

    if (title !== null) {
        metricNode.setAttribute('title', title);
    }
}

(async function() {
    var app = (await browser.runtime.getBackgroundPage()).pluginApp;
    var currentSession = await app.getCurrentSession();
    var countWebsites = await db.countWebsitesBySessionId(currentSession.id);
    var websites = await db.getWebsitesBySessionId(currentSession.id);
    var currentTab = await app.getCurrentTab();
    var currentTabWebsite = await app.getCurrentWebsite();

    showEmptyOrNonEmpty(websiteNode, currentTabWebsite !== null);
    showEmptyOrNonEmpty(sessionNode, countWebsites > 0);

    if (currentTabWebsite !== null) {
        if (currentTab.favIconUrl !== undefined) {
            let favicon = document.createElement('img');
            favicon.src = currentTab.favIconUrl;

            document.getElementById('website-title-favicon')
                .appendChild(favicon);
        }

        document.getElementById('website-title-title')
            .textContent = currentTab.title;
        document.getElementById('website-title-title')
            .setAttribute('title', currentTab.url);

        let co2 = formatter.formatMass(
            await calculator.getWebsiteCarbonEmission(currentTabWebsite.id),
            'g');

        let equi =
            await calculator.getWebsiteEquivalentEmission(currentTabWebsite.id);
        let format_equi =
            formatter.formatUnit(equi.value, equi.unit, equi.utype);

        let requests = await db.countAssetsByWebsiteId(currentTabWebsite.id);

        let data = formatter.formatSize(
            await calculator.getWebsiteTotalDataSize(currentTabWebsite.id),
            'B');

        setMetric(websiteNode, 'co2', co2.value, co2.unit);
        setMetric(
            websiteNode,
            'equivalent',
            format_equi.value,
            format_equi.unit,
            equi.type);
        setMetric(websiteNode, 'count-requests', requests);
        setMetric(websiteNode, 'data', data.value, data.unit);

        document.getElementById('website-details-link').setAttribute('href',
            'website.html?id=' + currentTabWebsite.id);
    }

    if (countWebsites > 0) {
        let equi =
            await calculator.getSessionEquivalentEmission(currentSession.id);
        let format_equi =
            formatter.formatUnit(equi.value, equi.unit, equi.utype);

        let co2 = formatter.formatMass(
            await calculator.getSessionCarbonEmission(currentSession.id), 'g');

        let duration = formatter.formatDuration(
            (new Date() - websites[0].date) / 1000,
            's');
        let fullTime = formatter.formatDateTime(new Date(websites[0].date));

        let data = formatter.formatSize(
            await calculator.getSessionTotalDataSize(currentSession.id), 'B');

        setMetric(sessionNode, 'co2', co2.value, co2.unit);
        setMetric(
            sessionNode,
            'equivalent',
            format_equi.value,
            format_equi.unit,
            equi.type);
        setMetric(sessionNode, 'count-websites', countWebsites);
        setMetric(
            sessionNode,
            'time',
            duration.value,
            duration.unit,
            null,
            fullTime);
        setMetric(sessionNode, 'data', data.value, data.unit);

        document.getElementById('session-details-link').setAttribute('href',
            'session.html?id=' + currentSession.id);
    }
}());
