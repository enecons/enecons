import 'bootstrap';
import Chart from 'chart.js';

import Db from '../db/db.js';
import Calculator from '../calculator.js';
import Formatter from '../formatter.js';

var db = new Db();
var calculator = new Calculator(db);
var formatter = new Formatter();

async function buildChart(timeWindow) {
    var countWebsites = 0;
    var datas = [];
    var countHTTP = 0;
    var totalProduction = 0;
    var totalSize = 0;
    var firstDate = 0;
    var lastDate = 0;
    var labels = [];
    var max = 0;
    var today = new Date();
    var title = '';

    if (timeWindow === 'day') {
        max = 24;
        title = ' today, ' + formatter.formatDate(new Date());
    } else if (timeWindow === 'week') {
        max = 7;
        title = ' this week';
    } else if (timeWindow === 'month') {
        max = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
        title = ' this month';
    } else if (timeWindow === 'year') {
        max = 12;
        title = ' this year';
        labels = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];
    }

    for (var i = 0; i < max; i++) {
        let start;
        let end;

        if (timeWindow === 'day') {
            start = new Date(today);
            start.setHours(i, 0, 0);
            end = new Date(today);
            end.setHours(i + 1, 0, 0);
        } else if (timeWindow === 'week') {
            start = new Date(today);
            start.setDate(today.getDate() - today.getDay() + i);
            start.setHours(0, 0, 0);
            end = new Date(start);
            end.setDate(start.getDate() + 1);
        } else if (timeWindow === 'month') {
            start = new Date(today);
            start.setDate(i + 1);
            start.setHours(0, 0, 0);
            end = new Date(start);
            end.setDate(i + 2);
        } else if (timeWindow === 'year') {
            start = new Date(today.getFullYear(), i, 1);
            end = new Date(today.getFullYear(), i + 1, 1);
        }

        let timeWindowData = await calculator
            .getTimeWindowStatistics(start, end);

        countWebsites += timeWindowData.countWebsites;
        countHTTP += timeWindowData.countHTTP;
        totalProduction += timeWindowData.Co2;
        totalSize += timeWindowData.size;

        if (timeWindowData.countWebsites > 0) {
            if (firstDate === 0) {
                firstDate = timeWindowData.firstWebsite;
            }
            lastDate = timeWindowData.lastWebsite;
        }

        if (timeWindow === 'day') {
            labels.push(
                start.toLocaleString([], {hour: '2-digit', minute: 'numeric'})
            );
        } else if (timeWindow === 'month') {
            labels.push(start.toLocaleString([], {day: '2-digit'}));
        } else if (timeWindow === 'week') {
            labels.push(start.toLocaleString([], {weekday: 'long'}));
        }

        datas.push(timeWindowData.Co2.toFixed(2));
    }

    var config = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Carbon produced',
                data: datas,
                backgroundColor: 'rgba(0, 123, 255, 0.37)',
                borderWidth: 1,
            }],
        },
        options: {
            title: {
                display: 'true',
                position: 'top',
                text: 'Carbon produced' + title,
            },
            scales: {
                yAxis: [{
                    ticks: {
                        beginAtZero: 'true',
                    },
                }],
            },
        },
    };

    if (firstDate === 0) {
        document.getElementById('start-time-' + timeWindow).appendChild(
            document.createElement('em')
        ).textContent = 'N/A';
    } else {
        document.getElementById('start-time-' + timeWindow).textContent =
            formatter.formatDateTime(new Date(firstDate));
    }
    if (lastDate === 0) {
        document.getElementById('end-time-' + timeWindow).appendChild(
            document.createElement('em')
        ).textContent = 'N/A';
    } else {
        document.getElementById('end-time-' + timeWindow).textContent =
            formatter.formatDateTime(new Date(lastDate));
    }

    var carbon = formatter.formatMass(totalProduction);
    var size = formatter.formatSize(totalSize);
    document.getElementById('nbVisited-' + timeWindow).textContent =
        countWebsites.toLocaleString();
    document.getElementById('carbon-' + timeWindow).textContent =
        carbon.value + carbon.unit;
    document.getElementById('nbHTTP-' + timeWindow).textContent = countHTTP
        .toLocaleString();
    document.getElementById('data-tx-' + timeWindow).textContent =
        size.value + size.unit;

    var ctx = document.getElementById('chart_' + timeWindow).getContext('2d');
    /* eslint-disable no-unused-vars */
    var chart = new Chart(ctx, config);
}

buildChart('day');
buildChart('week');
buildChart('month');
buildChart('year');
