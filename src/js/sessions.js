import 'bootstrap';

import Db from '../db/db.js';
import Formatter from '../formatter.js';

var db = new Db();
var formatter = new Formatter();

var naElement = document.createElement('em');
naElement.textContent = 'N/A';

var buttonMoreInfo = document.createElement('a');
buttonMoreInfo.setAttribute('class', 'btn btn-info');
buttonMoreInfo.textContent = 'More Information';

(async function() {
    var sessions = await db.getAllSessions();
    var list = document.getElementById('list');

    for (const session of sessions) {
        var newLine = list.insertRow(0);
        var websites = await db.getWebsitesBySessionId(session.id);
        var countWebsites = await db.countWebsitesBySessionId(session.id);

        newLine.insertCell(0).textContent = session.id;

        if (countWebsites < 1) {
            newLine.insertCell(1).appendChild(naElement.cloneNode(true));
            newLine.insertCell(2).appendChild(naElement.cloneNode(true));
        } else {
            newLine.insertCell(1).textContent += formatter.formatDateTime(
                new Date(websites[0].date)
            );
            newLine.insertCell(2).textContent += formatter.formatDateTime(
                new Date(websites[countWebsites - 1].date)
            );
        }

        newLine.insertCell(3).textContent = countWebsites;

        let button = buttonMoreInfo.cloneNode(true);
        button.setAttribute(
            'href',
            'session.html?id=' + session.id);
        newLine.insertCell(4).appendChild(button);
    }
}());
