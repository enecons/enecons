import 'bootstrap';

import Db from '../db/db.js';
import Calculator from '../calculator.js';
import Formatter from '../formatter.js';

var db = new Db();
var calculator = new Calculator(db);
var formatter = new Formatter();

var naElement = document.createElement('em');
naElement.textContent = 'N/A';

(async function() {
    var parsedURL = new URL(window.location.href);
    var sessionID = parseInt(parsedURL.searchParams.get('id'), 10);
    var websites = await db.getWebsitesBySessionId(sessionID);
    var carbon = formatter.formatMass(await calculator
        .getSessionCarbonEmission(sessionID));
    var countWebsites = websites.length;
    var nbHTTP = 0;
    var totalSize = 0;

    document.getElementById('sessID').textContent = sessionID;
    if (countWebsites > 0) {
        document.getElementById('start-time').textContent = formatter
            .formatDateTime(new Date(websites[0].date));
        document.getElementById('end-time').textContent = formatter
            .formatDateTime(new Date(websites[countWebsites - 1].date));

        for (const website of websites) {
            let assets = await db.getAssetsByWebsiteId(website.id);
            let newLine = document.getElementById('list').insertRow(0);
            let d = newLine.insertCell(0);
            let u = newLine.insertCell(1);
            let c = newLine.insertCell(2);
            let more = newLine.insertCell(3);
            let p = document.createElement('p');
            let a = document.createElement('a');
            let websiteEmission = formatter.formatMass(
                await calculator.getWebsiteCarbonEmission(website.id)
            );

            d.textContent = formatter.formatDateTime(new Date(website.date));
            p.textContent = website.url;
            p.className = 'overflow-td';
            u.appendChild(p);
            c.textContent = websiteEmission.value + websiteEmission.unit;

            a.title = 'More Information';
            a.textContent = 'More Information';
            a.href = 'website.html?id=' + website.id;
            a.className = 'btn btn-info';
            more.appendChild(a);

            for (const asset of assets) {
                totalSize += asset.size;
            }
            nbHTTP += assets.length;
        }
    } else {
        document.getElementById('start-time')
            .appendChild(naElement.cloneNode(true));
        document.getElementById('end-time')
            .appendChild(naElement.cloneNode(true));
    }
    var formattedSize = formatter.formatSize(totalSize);
    document.getElementById('nbVisited').textContent = countWebsites;
    document.getElementById('carbon').textContent = carbon.value + carbon.unit;
    document.getElementById('nbHTTP').textContent = nbHTTP;
    document.getElementById('data_tx').textContent = formattedSize.value +
        formattedSize.unit;
}());
