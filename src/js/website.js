import 'bootstrap';

import Db from '../db/db.js';
import Calculator from '../calculator.js';
import Formatter from '../formatter.js';

var db = new Db();
var formatter = new Formatter();
var calculator = new Calculator(db);
var parsedURL = new URL(window.location.href);
var websiteId = parseInt(parsedURL.searchParams.get('id'), 10);

(async function() {
    var website = await db.getWebsiteByWebsiteId(websiteId);
    var assets = await db.getAssetsByWebsiteId(websiteId);
    var networkSize = await calculator.getWebsiteTotalDataSize(websiteId);

    var date = formatter.formatDateTime(new Date(website[0].date));
    var co2 = formatter.formatMass(
        await calculator.getWebsiteCarbonEmission(websiteId),
        'g');
    var networkSizeFormated = formatter.formatSize(networkSize, 'B');
    var fObj = null;

    var totalAssetSize = 0;
    for (const asset of assets) {
        let newLine = document.getElementById('asset-list').insertRow(0);
        let s = newLine.insertCell(0);
        let t = newLine.insertCell(1);
        let u = newLine.insertCell(2);
        var p = document.createElement('p');

        totalAssetSize += asset.size;
        if (!asset.cached){
            fObj = formatter.formatSize(asset.size);
        }

        p.textContent = asset.url;
        p.className = 'overflow-td';

        s.textContent = (asset.cached ? 'cached' : fObj.value + fObj.unit);
        t.textContent = asset.type;
        u.appendChild(p);
    }

    var savedWithCache = formatter.formatSize(
        totalAssetSize - networkSize, 'B');

    document.getElementById('sess-link').href += '' + website[0].session_id;
    document.getElementById('sessID').textContent = '' + website[0].session_id;
    document.getElementById('websID').textContent = websiteId;
    document.getElementById('curr-wp-url').textContent = website[0].url;
    document.getElementById('visitOn').textContent = date;
    document.getElementById('carbon').textContent = co2.value + '' + co2.unit;
    document.getElementById('networkSize').textContent =
        networkSizeFormated.value + ' ' + networkSizeFormated.unit;
    document.getElementById('savedCache').textContent =
        savedWithCache.value + ' ' + savedWithCache.unit;
}());
