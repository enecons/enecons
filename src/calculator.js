export default class Calculator {
    constructor(db) {
        this.db = db;
        this.co2PerKwh = 45;
        this.kwhPerByte = (5.12 / 1000000000);
        this.equivalence = [
            {type: 'of your lamp light on', value: 0.387, unit: 'm',
                utype: 'duration'},
            {type: 'of using a microwave', value: 0.6, unit: 'm',
                utype: 'duration'},
            {type: 'of breathing', value: 0.0625, unit: 'm', utype: 'duration'},
            {type: 'of watching TV', value: 1.35, unit: 'm', utype: 'duration'},
        ];
        Object.freeze(this.equivalence);
    }

    convertSizeToCo2(size) {
        return size * (this.co2PerKwh * this.kwhPerByte);
    }

    async getWebsiteCarbonEmission(website_id) {
        return this.convertSizeToCo2(
            await this.getWebsiteTotalDataSize(website_id));
    }

    async getSessionCarbonEmission(session_id) {
        return this.convertSizeToCo2(
            await this.getSessionTotalDataSize(session_id));
    }

    async getTimeWindowStatistics(start, end) {
        var websites = await this.db.getWebsitesByTimeWindow(start, end);
        var emission = 0;
        var countHTTP = 0;
        var size = 0;
        var firstDateWebsite = 0;
        var lastDateWebsite = 0;

        if (websites.length > 0) {
            firstDateWebsite = websites[0].date;
            lastDateWebsite = websites[websites.length - 1].date;
        }
        for (const website of websites) {
            let assets = await this.db.getAssetsByWebsiteId(website.id);
            countHTTP += assets.length;
            emission += await this.getWebsiteCarbonEmission(website.id);

            for (const asset of assets) {
                size += asset.size;
            }
        }

        return {
            countWebsites: websites.length,
            firstWebsite: firstDateWebsite,
            lastWebsite: lastDateWebsite,
            Co2: emission,
            size: size,
            countHTTP: countHTTP,
        };
    }

    async getSessionEquivalentEmission(session_id) {
        var random = Math.floor(Math.random() * this.equivalence.length);
        var co2 = await this.getSessionCarbonEmission(session_id);
        var emission = co2 / this.equivalence[random].value;

        return {
            type: this.equivalence[random].type,
            value: emission,
            unit: this.equivalence[random].unit,
            utype: this.equivalence[random].utype,
        };
    }

    async getWebsiteEquivalentEmission(website_id) {
        var random = Math.floor(Math.random() * this.equivalence.length);
        var co2 = await this.getWebsiteCarbonEmission(website_id);
        var emission = co2 / this.equivalence[random].value;

        return {
            type: this.equivalence[random].type,
            value: emission,
            unit: this.equivalence[random].unit,
            utype: this.equivalence[random].utype,
        };
    }

    async getWebsiteTotalDataSize(website_id) {
        var assets = await this.db.getAssetsByWebsiteId(website_id);

        return assets.reduce(function(total, asset) {
            return total + asset.getNetworkSize();
        }, 0);
    }

    async getSessionTotalDataSize(session_id) {
        var websites = await this.db.getWebsitesBySessionId(session_id);

        var total = 0;
        for (const website of websites) {
            total += await this.getWebsiteTotalDataSize(website.id);
        }

        return total;
    }
}
