import Db from './db/db.js';
import Session from './db/session.js';

class App {
    constructor() {
        this.current_session = null;
        this.website_on_tabs = {};
        this.db = new Db();
    }

    async getCurrentSession() {
        if (this.current_session === null) {
            this.current_session = this.db.save(new Session());
        }

        return this.current_session;
    }

    hasWebsiteOnTab(tab_id) {
        return tab_id in this.website_on_tabs;
    }

    async getWebsiteOnTab(tab_id) {
        return this.website_on_tabs[tab_id];
    }

    setWebsiteOnTab(tab_id, website) {
        this.website_on_tabs[tab_id] = website;
    }

    async getCurrentTab() {
        var currentTabs = await browser.tabs.query({
            active: true,
            currentWindow: true,
        });

        if (currentTabs.length > 0) {
            return currentTabs[0];
        }

        return null;
    }

    async getCurrentWebsite() {
        var currentTab = await this.getCurrentTab();

        if (this.hasWebsiteOnTab(currentTab.id)) {
            return this.getWebsiteOnTab(currentTab.id);
        }

        return null;
    }
}

var app = new App();
export default app;
